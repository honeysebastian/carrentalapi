package CarTests;

import java.nio.file.*;
import java.util.List;
import java.util.ArrayList;

import CarFramework.*;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.google.gson.Gson;

/*This class has test methods for CarRentalAPI 
*
** @author : Honey
* 
* @Last updated  : 2019-02-15 
* */
public class CarRentalsTest extends Car {

//GSON is used for Parsing JSON into Java using the class :Car
	Gson gson = new Gson();
	String serverResponseFile,serverResponseJson ;


	//
	@BeforeClass 
	public void readJSON() throws Exception {
		//getting API response
		//reading the Json response from file
		serverResponseFile = "src/main/resources/response.json";
		serverResponseJson = new String(Files.readAllBytes(Paths.get(serverResponseFile)));

	}

	//test method (combined all the printing in one method)
	@Test
	public void findCars() throws Exception {


		Car[] cars = gson.fromJson(serverResponseJson, Car[].class);
		List<Car> blueTeslaCars = new ArrayList<Car>();
		Car lowestPerDayPrice = null, lowestPerDayPriceAfterDiscount = null, highestRevenue = null;

//Question 1: Print all the blue Teslas received in the web service response. Also print the notes
		for (Car car : cars) {


			if (CarUtils.matchMakeAndColor(car, "Tesla", "Blue")) {
				blueTeslaCars.add(car);
			}

//			Question 2: Return all cars which have the lowest per day rental cost for both cases:
			
			//	a. Price only
			
			if (lowestPerDayPrice == null || lowestPerDayPrice.getPerdayrent().Price > car.getPerdayrent().Price) {
				lowestPerDayPrice = car;
			}

			//b. Price after discounts
			if (lowestPerDayPriceAfterDiscount == null || CarUtils.getPriceAfterDiscount(lowestPerDayPriceAfterDiscount) > CarUtils.getPriceAfterDiscount(car)) {
				lowestPerDayPriceAfterDiscount = car;
			}

//			Question 3: Find the highest revenue generating car. year over year maintenance cost + depreciation is the total expense per car for the full year for the rental car company.
			
			if (highestRevenue == null || CarUtils.getYearlyProfit(car) > CarUtils.getYearlyProfit(highestRevenue)) {
				highestRevenue = car;
			}

		}

//printing all the results using common method basicInfoString with different parameters
		
		System.out.println("**List of blue Tesla cars**");
		for (Car blueTeslaCar : blueTeslaCars) 
			System.out.println(CarUtils.basicInfoString(blueTeslaCar));
			
		System.out.println("**Lowest per-day price**");
		System.out.println(CarUtils.basicInfoString(lowestPerDayPrice));

		System.out.println("**Lowest per-day price after discount**");
		System.out.println(CarUtils.basicInfoString(lowestPerDayPriceAfterDiscount));

		System.out.println("**Highest revenue generating car**");
		System.out.println(CarUtils.basicInfoString(highestRevenue));
		
		
	}
}