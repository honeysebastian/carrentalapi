package CarFramework;

public class Car {
	public String make;
	public String model;
	public String vin;

	private Metadata metadata;
	private Perdayrent perdayrent;
	private Metrics metrics;

	public Metadata getMetadata() {
		return metadata; 
	} 
	public Perdayrent getPerdayrent() {
		return perdayrent;
	} 
	public Metrics getMetrics() {
		return metrics; 
	} 

	public static class Metadata {
		public String Color;
		public String Notes;
	}

	 public  static class Perdayrent {
		public float Price;
		public float Discount;
	}

	 public static class Metrics {
		public float yoymaintenancecost;
		public float depreciation;

		private Rentalcount rentalcount;

		public Rentalcount getRentalcount() {
			return rentalcount; 
		} 

		public static class Rentalcount {
			public int lastweek;
			public int yeartodate;
		}
	}
}

