package CarFramework;

/*This class contains frequently used methods which can be used in the CarRentalAPI Test framework.
 *
 ** @author : Honey
* 
* @Last updated  : 2019-02-15 
 * */
public class CarUtils {
	CarUtils () {
	}
	
	//method to find cars with specific make and color 
	public static boolean matchMakeAndColor(Car car, String make, String color) {
		if (car.make.equalsIgnoreCase(make) && car.getMetadata().Color.equalsIgnoreCase(color)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	//method to calculate per day rent after deducting discount
	public static float getPriceAfterDiscount(Car car) {
		float priceAfterDiscount;
		priceAfterDiscount = car.getPerdayrent().Price - car.getPerdayrent().Discount;
		
		return priceAfterDiscount;
	}
	
	//method to calculate annual profit of car(total income- total expense)
	public static float getYearlyProfit(Car car) {
		float profit;
		float expense = car.getMetrics().yoymaintenancecost + car.getMetrics().depreciation;
		float income = getPriceAfterDiscount(car) * car.getMetrics().getRentalcount().yeartodate;
		
		profit = income - expense;
		
		return profit;
	}
	
	//method to print the car info based on the parameter.
	public static String basicInfoString(Car car) {
		String basicInfo = "";

		basicInfo += "Car: "+car.make+" "+car.model+", ";
		basicInfo += "VIN: "+car.vin+", ";
		basicInfo += "Notes: "+car.getMetadata().Notes+"\n";
		
		return basicInfo;
	}
}